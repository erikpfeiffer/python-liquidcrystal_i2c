#!/usr/bin/env python

from setuptools import setup

setup(name='liquidcrystal_i2c',
      version='0.3',
      description='Library for the LCD-Display "LCM1602 IIC V2"',
      author='Erik Pfeiffer',
      url='https://gitlab.com/erikpfeiffer/python-liquidcrystal_i2c',
      packages=['liquidcrystal_i2c'],
      license='MIT License',
      requires='pysmbus',
      zip_safe=True)
